use gst::prelude::*;
use gstreamer as gst;

use gstspleeter;

fn init() {
    use std::sync::Once;
    static INIT: Once = Once::new();

    INIT.call_once(|| {
        gst::init().unwrap();
        gstspleeter::plugin_register_static().expect("spleeter tests");
    });
}

fn test_pipeline(pipeline_desc: &str) {
    init();

    let pipeline = gst::parse_launch(pipeline_desc).unwrap();

    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    let bus = pipeline.get_bus().unwrap();
    for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
        use gst::MessageView;
        match msg.view() {
            MessageView::Error(err) => {
                eprintln!(
                    "Error received from element {:?}: {}",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error()
                );
                eprintln!("Debugging information: {:?}", err.get_debug());
                unreachable!();
            }
            MessageView::Eos(..) => break,
            _ => (),
        }
    }

    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");
}

#[test]
fn test_spleeter_all_linked() {
    test_pipeline("audiotestsrc num-buffers=1 ! spleeter name=s s.accompaniment ! queue ! fakesink s.vocals ! queue ! fakesink");
}

#[test]
fn test_spleeter_one_linked() {
    test_pipeline(
        "audiotestsrc num-buffers=1 ! spleeter model=4-stems name=s s.drums ! queue ! fakesink",
    );
}
