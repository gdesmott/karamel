// Copyright (C) 2019 Guillaume Desmottes <gdesmott@gnome.org>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// Ideally we'd use Tensorflow's Rust bindings but the Estimator API is
// not binded atm: https://github.com/tensorflow/rust/issues/227

use libc;
use std::collections::HashMap;
use std::ffi::CStr;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::atomic::AtomicPtr;

use anyhow::{bail, Result};
use pkg_config;
use pyo3::prelude::*;
use pyo3::types::{PyBytes, PyString};

use crate::spleeter::{Model, StemType};

const PY_CODE: &str = "
# silent numpy
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# silent tf
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from spleeter.separator import Separator
import numpy as np

def run_spleeter(model, channels, data):
  waveform = np.frombuffer(data, dtype='<f4').reshape(-1, channels)
  separator = Separator(model)

  result = separator.separate(waveform)
  # Flatten the returned list to reduce memory usage in Rust
  for k in result:
    result[k] = [item for sublist in result[k] for item in sublist]

  return result
";

pub struct PythonWrapper {
    libpython: AtomicPtr<libc::c_void>,
}

impl Drop for PythonWrapper {
    fn drop(&mut self) {
        unsafe {
            libc::dlclose(*self.libpython.get_mut());
        }
    }
}

fn find_libpython(lib: &str, so: &str) -> Result<PathBuf> {
    let libdir = pkg_config::get_variable(lib, "libdir")?;
    let mut libpython = Path::new(&libdir).to_path_buf();
    libpython.push(so);
    Ok(libpython)
}

impl PythonWrapper {
    pub fn new() -> Result<PythonWrapper> {
        // Manually load libpython, this is required a our gst plugin is likely
        // loaded dynamically.
        let libpython = find_libpython("python3-embed", "libpython3.so\0")
            .or_else(|_| find_libpython("python-3.7m", "libpython3.7m.so\0"))?;
        let libpython = libpython.into_os_string().into_string().unwrap();

        let handle = unsafe {
            let handle = libc::dlopen(
                libpython.as_ptr() as *const i8,
                libc::RTLD_GLOBAL | libc::RTLD_LAZY,
            );
            if handle.is_null() {
                let err = libc::dlerror();
                let c_str: &CStr = CStr::from_ptr(err);

                bail!("Failed to load libpython: {}", c_str.to_str()?);
            }
            handle
        };

        Ok(PythonWrapper {
            libpython: AtomicPtr::new(handle),
        })
    }

    fn run<'a>(
        &'a self,
        py: Python<'a>,
        nb: Model,
        channels: i32,
        input: &[u8],
    ) -> PyResult<HashMap<&str, Vec<f32>>> {
        let module = PyModule::from_code(py, PY_CODE, "wrapper.rs", "wrapper")?;

        let model = match nb {
            Model::_2Stems => "spleeter:2stems",
            Model::_4Stems => "spleeter:4stems",
            Model::_5Stems => "spleeter:5stems",
        };

        let model = PyString::new(py, &model);
        let channels = channels.to_object(py);
        let input = PyBytes::new(py, &input);
        let result: HashMap<&str, Vec<f32>> = module
            .call1("run_spleeter", (model, channels, input))?
            .extract()?;
        Ok(result)
    }

    pub fn spleeter(
        &self,
        nb: Model,
        channels: i32,
        input: &[u8],
    ) -> Result<HashMap<StemType, Vec<u8>>> {
        let gil = Python::acquire_gil();
        let py = gil.python();

        let result = match self.run(py, nb, channels, input) {
            Err(e) => {
                // We can't display python error type via ::std::fmt::Display,
                // so print error here manually.
                e.print_and_set_sys_last_vars(py);
                bail!("Failed to load python code");
            }
            Ok(result) => result,
        };

        // Copy result from Python and convert f32 to u8
        let mut copy: HashMap<StemType, Vec<u8>> = HashMap::new();
        for (key, val) in result.iter() {
            let t = StemType::from_str(key)?;
            let mut data: Vec<u8> = Vec::new();
            val.iter().for_each(|v| {
                data.extend(v.to_le_bytes().iter().copied());
            });
            copy.insert(t, data);
        }
        Ok(copy)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_2_stems() {
        let wrapper = PythonWrapper::new().unwrap();

        let input: [u8; 32] = [0; 32];
        let result = wrapper.spleeter(Model::_2Stems, 2, &input).unwrap();
        assert_eq!(result.len(), 2);
        assert_eq!(result.get(&StemType::Accompaniment).unwrap(), &input);
        assert_eq!(result.get(&StemType::Vocals).unwrap(), &input);
    }

    #[test]
    fn test_4_stems() {
        let wrapper = PythonWrapper::new().unwrap();
        let input: [u8; 32] = [0; 32];
        let result = wrapper.spleeter(Model::_4Stems, 2, &input).unwrap();
        assert_eq!(result.len(), 4);
        assert_eq!(result.get(&StemType::Bass).unwrap(), &input);
        assert_eq!(result.get(&StemType::Drums).unwrap(), &input);
        assert_eq!(result.get(&StemType::Other).unwrap(), &input);
        assert_eq!(result.get(&StemType::Vocals).unwrap(), &input);
    }

    #[test]
    fn test_5_stems() {
        let wrapper = PythonWrapper::new().unwrap();
        let input: [u8; 32] = [0; 32];
        let result = wrapper.spleeter(Model::_5Stems, 2, &input).unwrap();
        assert_eq!(result.len(), 5);
        assert_eq!(result.get(&StemType::Bass).unwrap(), &input);
        assert_eq!(result.get(&StemType::Drums).unwrap(), &input);
        assert_eq!(result.get(&StemType::Other).unwrap(), &input);
        assert_eq!(result.get(&StemType::Vocals).unwrap(), &input);
        assert_eq!(result.get(&StemType::Piano).unwrap(), &input);
    }
}
