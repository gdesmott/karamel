// Copyright (C) 2019 Guillaume Desmottes <gdesmott@gnome.org>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// Element wrapping Spleeter: Deezer's source separation library:
// https://github.com/deezer/spleeter
//
// Spleeter needs to be installed first:
// $ pip3 install spleeter
//
// Sample pipelines:
// $ gst-launch-1.0 filesrc location=audio_example.wav ! decodebin ! spleeter name=s s.accompaniment ! autoaudiosink
// $ gst-launch-1.0 filesrc location=audio_example.wav ! decodebin ! spleeter model=5-stems name=s s.piano ! wavenc ! filesink location=piano.wav

use glib::prelude::*;
use glib::subclass;
use glib::subclass::prelude::*;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gstreamer_audio as gst_audio;
use gstreamer_base as gst_base;

use std::collections::HashMap;
use std::i32;
use std::str::FromStr;
use std::sync::Mutex;

use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter};
use thiserror::Error;

use crate::wrapper::PythonWrapper;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, GEnum)]
#[repr(u32)]
#[genum(type_name = "GstSpleeterModel")]
pub enum Model {
    #[genum(name = "2 Stems", nick = "2-stems")]
    _2Stems,
    #[genum(name = "4 Stems", nick = "4-stems")]
    _4Stems,
    #[genum(name = "5 Stems", nick = "5-stems")]
    _5Stems,
}

static PROPERTIES: [subclass::Property; 1] = [subclass::Property("model", |name| {
    glib::ParamSpec::enum_(
        name,
        "Model",
        "Spleeter model used for splitting stems",
        Model::static_type(),
        Model::_2Stems as i32,
        glib::ParamFlags::READWRITE,
    )
})];

impl Model {
    fn stems(self) -> Vec<StemType> {
        match self {
            Model::_2Stems => vec![StemType::Accompaniment, StemType::Vocals],
            Model::_4Stems => vec![
                StemType::Vocals,
                StemType::Bass,
                StemType::Drums,
                StemType::Other,
            ],
            Model::_5Stems => vec![
                StemType::Vocals,
                StemType::Bass,
                StemType::Drums,
                StemType::Other,
                StemType::Piano,
            ],
        }
    }
}

#[derive(Debug, Clone)]
struct Settings {
    model: Model,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            model: Model::_2Stems,
        }
    }
}

struct State {
    adapter: gst_base::UniqueAdapter,
    duration: gst::ClockTime,
    wrapper: PythonWrapper,
    caps: Option<gst::Caps>,
    srcpads: HashMap<StemType, gst::Pad>,
}

impl Default for State {
    fn default() -> Self {
        Self {
            adapter: gst_base::UniqueAdapter::new(),
            duration: gst::ClockTime::none(),
            wrapper: PythonWrapper::new().expect("Failed to load Python"),
            caps: None,
            srcpads: HashMap::new(),
        }
    }
}

struct Spleeter {
    sinkpad: gst::Pad,
    settings: Mutex<Settings>,
    state: Mutex<State>,
}

lazy_static! {
    static ref CAT: gst::DebugCategory = gst::DebugCategory::new(
        "spleeter",
        gst::DebugColorFlags::empty(),
        Some("Spleeter debug category"),
    );
}

#[derive(Debug, Eq, PartialEq, Hash, EnumIter, Display, Clone, Copy)]
pub enum StemType {
    #[strum(serialize = "vocals")]
    Vocals,
    #[strum(serialize = "accompaniment")]
    Accompaniment,
    #[strum(serialize = "drums")]
    Drums,
    #[strum(serialize = "other")]
    Other,
    #[strum(serialize = "bass")]
    Bass,
    #[strum(serialize = "piano")]
    Piano,
}

#[derive(Error, Debug)]
pub enum StemTypeError {
    #[error("Invalid stem type")]
    InvalidStemType,
}

impl FromStr for StemType {
    type Err = StemTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "vocals" => Ok(StemType::Vocals),
            "accompaniment" => Ok(StemType::Accompaniment),
            "drums" => Ok(StemType::Drums),
            "other" => Ok(StemType::Other),
            "bass" => Ok(StemType::Bass),
            "piano" => Ok(StemType::Piano),
            _ => Err(StemTypeError::InvalidStemType),
        }
    }
}

impl Spleeter {
    fn set_pad_functions(sinkpad: &gst::Pad) {
        sinkpad.set_chain_function(|pad, parent, buffer| {
            Spleeter::catch_panic_pad_function(
                parent,
                || Err(gst::FlowError::Error),
                |spleeter, element| spleeter.sink_chain(pad, element, buffer),
            )
        });

        sinkpad.set_event_function(|pad, parent, event| {
            Spleeter::catch_panic_pad_function(
                parent,
                || false,
                |spleeter, element| spleeter.sink_event(pad, element, event),
            )
        });
    }

    fn sink_chain(
        &self,
        pad: &gst::Pad,
        _element: &gst::Element,
        buffer: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        gst_log!(CAT, obj: pad, "Handling buffer {:?}", buffer);

        {
            // Need to accumulate the whole stream before splitting,
            // see https://github.com/deezer/spleeter/issues/192
            let mut state = self.state.lock().unwrap();
            let duration = buffer.get_duration();
            if duration.is_some() {
                if state.duration.is_none() {
                    state.duration = duration;
                } else {
                    state.duration += duration;
                }
            }
            state.adapter.push(buffer);
        }

        Ok(gst::FlowSuccess::Ok)
    }

    fn sink_event(&self, pad: &gst::Pad, element: &gst::Element, event: gst::Event) -> bool {
        use gst::EventView;

        gst_log!(CAT, obj: pad, "Handling event {:?}", event);

        match event.view() {
            EventView::Caps(caps) => {
                let mut state = self.state.lock().unwrap();
                let caps = caps.get_caps_owned();
                state.caps.replace(caps);
            }
            EventView::Eos(_) => {
                gst_debug!(CAT, obj: element, "Received EOS, split input");

                match self.split_input(element) {
                    Ok(_) => {
                        gst_debug!(CAT, obj: element, "Successfully pushed splitted stems");
                    }
                    Err(gst::FlowError::Flushing) => {
                        gst_debug!(CAT, obj: element, "Flushing");
                    }
                    Err(gst::FlowError::Eos) => {
                        gst_debug!(CAT, obj: element, "EOS");
                    }
                    Err(err) => {
                        gst_error!(CAT, obj: element, "Got error {}", err);
                        gst_element_error!(
                            element,
                            gst::StreamError::Failed,
                            ("Internal data stream error"),
                            ["streaming stopped, reason {}", err]
                        );
                    }
                }
            }
            _ => {}
        };

        for srcpad in element.iterate_src_pads() {
            let srcpad = srcpad.unwrap();
            srcpad.push_event(event.clone());
        }

        true
    }

    fn split_input(&self, element: &gst::Element) -> Result<gst::FlowSuccess, gst::FlowError> {
        let mut ret: Result<gst::FlowSuccess, gst::FlowError> = Ok(gst::FlowSuccess::Ok);

        let model = self.settings.lock().unwrap().model;
        let (mut stems, pts, dts, duration) = {
            let mut state = self.state.lock().unwrap();

            let available = state.adapter.available();
            if available == 0 {
                return ret;
            }

            let caps = state.caps.as_ref().unwrap();
            let s = caps.get_structure(0).unwrap();
            let channels: i32 = s.get_some("channels").unwrap();

            let duration = state.duration;
            state.duration = gst::ClockTime::none();
            let (pts, _) = state.adapter.prev_pts();
            let (dts, _) = state.adapter.prev_dts();
            let input = state.adapter.take_buffer(available).unwrap();
            let map = input.map_readable().unwrap();

            let stems = match state.wrapper.spleeter(model, channels, map.as_slice()) {
                Err(_) => return Err(gst::FlowError::Error),
                Ok(result) => result,
            };

            (stems, pts, dts, duration)
        };

        for stem in model.stems().iter() {
            let data = stems.remove(&stem).expect("Missing stem");
            let mut output = gst::Buffer::from_slice(data);
            {
                let output = output.get_mut().unwrap();
                output.set_pts(pts);
                output.set_dts(dts);
                output.set_flags(gst::BufferFlags::DISCONT);
                output.set_duration(duration);
            }

            let srcpad = {
                let state = self.state.lock().unwrap();
                let srcpad = state.srcpads.get(&stem).expect("missing src pad");
                srcpad.clone()
            };

            if !srcpad.is_linked() {
                gst_info!(
                    CAT,
                    obj: element,
                    "Pad {} not linked, ignore stem {}",
                    srcpad.get_name(),
                    stem
                );
                continue;
            }
            gst_info!(CAT, obj: element, "Pushing to pad {}", srcpad.get_name());

            // return the latest not Ok result as tee does
            let res = srcpad.push(output);
            match res {
                Ok(gst::FlowSuccess::Ok) => {}
                Ok(_) | Err(_) => ret = res,
            };
        }
        ret
    }
}

impl ObjectSubclass for Spleeter {
    const NAME: &'static str = "Spleeter";
    type ParentType = gst::Element;
    type Instance = gst::subclass::ElementInstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new_with_class(klass: &subclass::simple::ClassStruct<Self>) -> Self {
        let templ = klass.get_pad_template("sink").unwrap();
        let sinkpad = gst::Pad::new_from_template(&templ, Some("sink"));

        Spleeter::set_pad_functions(&sinkpad);

        Self {
            sinkpad,
            settings: Mutex::new(Settings::default()),
            state: Mutex::new(State::default()),
        }
    }

    fn class_init(klass: &mut subclass::simple::ClassStruct<Self>) {
        klass.set_metadata(
            "Spleeter",
            "Effect/Audio",
            "Split input in multiple stems",
            "Guillaume Desmottes <gdesmott@gnome.org>",
        );

        let caps = gst::Caps::new_simple(
            "audio/x-raw",
            &[
                ("format", &gst_audio::AudioFormat::F32le.to_str()),
                ("layout", &"interleaved"),
                ("rate", &gst::IntRange::<i32>::new(1, i32::MAX)),
                ("channels", &gst::IntRange::<i32>::new(1, i32::MAX)),
            ],
        );

        for stem in StemType::iter() {
            let src_pad_template = gst::PadTemplate::new(
                &stem.to_string(),
                gst::PadDirection::Src,
                gst::PadPresence::Sometimes,
                &caps,
            )
            .unwrap();
            klass.add_pad_template(src_pad_template);
        }

        let sink_pad_template = gst::PadTemplate::new(
            "sink",
            gst::PadDirection::Sink,
            gst::PadPresence::Always,
            &caps,
        )
        .unwrap();
        klass.add_pad_template(sink_pad_template);

        klass.install_properties(&PROPERTIES);
    }
}

impl ObjectImpl for Spleeter {
    glib_object_impl!();

    fn constructed(&self, obj: &glib::Object) {
        self.parent_constructed(obj);

        let element = obj.downcast_ref::<gst::Element>().unwrap();
        element.add_pad(&self.sinkpad).unwrap();
    }

    fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
        let prop = &PROPERTIES[id];

        match *prop {
            subclass::Property("model", ..) => {
                let settings = self.settings.lock().unwrap();
                Ok(settings.model.to_value())
            }
            _ => unimplemented!(),
        }
    }

    fn set_property(&self, _obj: &glib::Object, id: usize, value: &glib::Value) {
        let prop = &PROPERTIES[id];

        match *prop {
            subclass::Property("model", ..) => {
                let mut settings = self.settings.lock().unwrap();
                let model = value.get().unwrap();
                settings.model = model.unwrap();
            }
            _ => unimplemented!(),
        }
    }
}

impl ElementImpl for Spleeter {
    fn change_state(
        &self,
        element: &gst::Element,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        if let gst::StateChange::NullToReady = transition {
            let model = self.settings.lock().unwrap().model;
            for stem in model.stems().iter() {
                let name = &stem.to_string();
                let templ = element.get_pad_template(name).unwrap();
                let srcpad = gst::Pad::new_from_template(&templ, Some(name));
                gst_debug!(CAT, obj: element, "Create src pad {}", srcpad.get_name());
                element.add_pad(&srcpad).unwrap();

                let mut state = self.state.lock().unwrap();
                state.srcpads.insert(*stem, srcpad);
            }
            element.no_more_pads();
        }

        let success = self.parent_change_state(element, transition)?;

        if let gst::StateChange::ReadyToNull = transition {
            let mut state = self.state.lock().unwrap();
            for (_, pad) in state.srcpads.drain() {
                element.remove_pad(&pad).unwrap_or_else(|_| {
                    gst_error!(CAT, obj: element, "Failed to remove pad {}", pad.get_name())
                });
            }
        }

        Ok(success)
    }
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "spleeter",
        gst::Rank::None,
        Spleeter::get_type(),
    )
}
