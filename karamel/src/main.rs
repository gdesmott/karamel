extern crate gstreamer as gst;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate log;

use anyhow;
use env_logger::{Builder, Env};
use std::path::PathBuf;
use structopt::StructOpt;

mod karamel;

use karamel::Karamel;

#[derive(StructOpt, Debug)]
#[structopt(name = "karamel")]
struct Opt {
    #[structopt(parse(from_os_str), help = "File to transform")]
    input: PathBuf,
}

fn main() -> Result<(), anyhow::Error> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();
    let opt = Opt::from_args();

    let ctx = glib::MainContext::default();
    ctx.push_thread_default();

    gst::init()?;
    gstspleeter::plugin_register_static()?;

    let karamel = Karamel::new(&ctx, opt.input)?;
    karamel.run()?;

    ctx.pop_thread_default();

    Ok(())
}
