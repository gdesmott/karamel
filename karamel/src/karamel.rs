use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use anyhow::Error;
use futures::prelude::*;
use glib;
use glib::subclass::prelude::*;
use gstreamer as gst;
use gstreamer_pbutils as pbutils;
use pbutils::prelude::*;
use thiserror;
use url::Url;

const DISCOVER_TIME_OUT: u64 = 5; //seconds
const DECODEBIN_MAX_QUEUE_TIME: u64 = 600; //seconds

#[derive(Clone, Debug, GBoxed)]
#[gboxed(type_name = "ErrorValue")]
struct ErrorValue(Arc<Mutex<Option<Error>>>);

#[derive(Debug, thiserror::Error)]
#[error("Missing element {0}")]
struct MissingElement(&'static str);

#[derive(Debug)]
pub struct Karamel {
    pipeline: gst::Element,
    loop_: glib::MainLoop,
    input: Url,
}

async fn message_handler(loop_: glib::MainLoop, bus: gst::Bus) {
    let mut messages = bus.stream();

    while let Some(msg) = messages.next().await {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => loop_.quit(),
            MessageView::Error(err) => {
                // FIXME: return error from run()
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                loop_.quit();
            }
            _ => (),
        }
    }
}

fn file_uri(path: &PathBuf) -> Url {
    Url::from_file_path(path.to_str().unwrap()).unwrap()
}

fn in_out_uris(input: &PathBuf) -> Result<(Url, Url), Error> {
    let input = input.as_path().canonicalize()?;
    let in_uri = file_uri(&input);

    let mut output = input.clone();
    let name = input.file_stem().unwrap().to_str().unwrap();
    let name = format!("{}-no-vocal", name);
    output.set_file_name(name);
    if let Some(ext) = input.extension() {
        output.set_extension(&ext);
    }
    let out_uri = file_uri(&output);

    Ok((in_uri, out_uri))
}

fn create_spleeter_bin() -> Result<gst::Bin, Error> {
    let spleeter =
        gst::ElementFactory::make("spleeter", None).map_err(|_| MissingElement("spleeter"))?;

    let bin = gst::Bin::new(Some("spleeter_bin"));
    bin.add(&spleeter)?;

    let spleeter_sink = spleeter
        .get_static_pad("sink")
        .expect("missing spleeter sink pad");
    let sink = gst::GhostPad::new(Some("sink"), &spleeter_sink)?;
    bin.add_pad(&sink)?;

    spleeter.set_state(gst::State::Ready)?;

    let acc = spleeter
        .get_static_pad("accompaniment")
        .expect("missing spleeter accompaniment pad");
    let sink = gst::GhostPad::new(Some("src"), &acc)?;
    bin.add_pad(&sink)?;

    Ok(bin)
}

fn create_pipeline(in_uri: &Url, out_uri: &Url) -> Result<gst::Element, Error> {
    let transcodebin = gst::ElementFactory::make("uritranscodebin", None)
        .map_err(|_| MissingElement("uritranscodebin"))?;
    let spleeter = create_spleeter_bin()?;

    transcodebin.set_property("source-uri", &in_uri.to_string())?;
    transcodebin.set_property("dest-uri", &out_uri.to_string())?;
    transcodebin.set_property("audio-filter", &spleeter)?;

    transcodebin
        .set_property("avoid-reencoding", &true)
        .unwrap();

    // FIXME: spleeter should split its input rather than forcing us to buffer everything to dead
    let bin: &gst::Bin = transcodebin.downcast_ref().unwrap();
    bin.connect_deep_element_added(|_, _, elt| {
        // FIXME: Is there a better way to check for the element type?
        if elt.get_type().to_string() == "GstDecodeBin" {
            elt.set_property(
                "max-size-time",
                &gst::ClockTime::from_seconds(DECODEBIN_MAX_QUEUE_TIME),
            )
            .unwrap();
            elt.set_property("max-size-bytes", &u32::max_value())
                .unwrap();
        }
    });

    Ok(transcodebin)
}

impl Karamel {
    pub fn new(ctx: &glib::MainContext, input: PathBuf) -> Result<Self, Error> {
        let (in_uri, out_uri) = in_out_uris(&input)?;
        debug!("Source: {}", in_uri);
        debug!("Output: {}", out_uri);

        let pipeline = create_pipeline(&in_uri, &out_uri)?;
        let loop_ = glib::MainLoop::new(Some(&ctx), false);

        let bus = pipeline.get_bus().unwrap();
        ctx.spawn_local(message_handler(loop_.clone(), bus));

        Ok(Self {
            pipeline,
            loop_,
            input: in_uri,
        })
    }

    fn detect_profile(&self) -> Result<pbutils::EncodingProfile, Error> {
        let timeout: gst::ClockTime = gst::ClockTime::from_seconds(DISCOVER_TIME_OUT);
        let discoverer = pbutils::Discoverer::new(timeout)?;
        let info = discoverer.discover_uri(&self.input.to_string())?;

        let container = info.get_stream_info().unwrap().get_caps().unwrap();
        let video = &info.get_video_streams()[0].get_caps().unwrap();
        let audio = &info.get_audio_streams()[0].get_caps().unwrap();

        // Keep only the media type of the caps
        let container = gst::Caps::new_simple(container.get_structure(0).unwrap().get_name(), &[]);
        // Keep 'mpegversion' if present on audio caps
        let s_audio = audio.get_structure(0).unwrap();
        let s_audio = gst::Structure::from_iter(
            s_audio.get_name(),
            s_audio.iter().filter(|(f, _)| *f == "mpegversion"),
        );
        let mut audio = gst::Caps::new_empty();
        audio.get_mut().unwrap().append_structure(s_audio);
        let video = gst::Caps::new_simple(video.get_structure(0).unwrap().get_name(), &[]);

        let audio_profile = pbutils::EncodingAudioProfileBuilder::new()
            .format(&audio)
            .presence(0)
            .build()
            .unwrap();

        let video_profile = pbutils::EncodingVideoProfileBuilder::new()
            .format(&video)
            .presence(0)
            .build()
            .unwrap();

        let container_profile = pbutils::EncodingContainerProfileBuilder::new()
            .name("container")
            .format(&container)
            .add_profile(&(video_profile))
            .add_profile(&(audio_profile))
            .build()
            .unwrap();

        Ok(container_profile.upcast())
    }

    pub fn run(&self) -> Result<(), Error> {
        // Keep the same stream formats
        let profile = self.detect_profile()?;
        self.pipeline.set_property("profile", &profile)?;

        debug!("Start transcoding");
        self.pipeline.set_state(gst::State::Playing)?;
        self.loop_.run();
        debug!("Done transcoding");
        self.pipeline.set_state(gst::State::Null)?;

        Ok(())
    }
}
